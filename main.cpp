/*
 * ФИО: Кудрявцев Андрей Витальевич
 * Группа: 271ПИ
 * Дата: 26.09.13
 * IDE: Eclipse IDE for C/C++ Developers
 * 		Build id: 20130614-0229
 */
#include <iostream>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
using namespace std;

void printArr(int *arr, int size);
void bubble(int *a, int n);
void bubbleI1(int *a, int n);
void bubbleI2(int *a, int n);
int *generateArr(int, int, int);
int *copyArr(int *arr, int size);
bool checkArr(int *arr, int size);

int main()
{
	int size, min, max, type;
	int *array, *copy;
	char answ='n';
	cout << "Hello! This is bubble sort!" << endl;
	while(true)
	{
		cout << "Let's generate array!" << endl << "Please input size of array: ";cin >> size;
		cout << "Please input minimal value: ";cin >> min;
		cout << "Please input maximal value: ";cin >> max;
		while(answ=='n')
		{
			array = generateArr(size, min, max);
			printArr(array, size);
			cout << "Do you like it?(y/n) ";cin >> answ;
		}
		answ = 'o';
		while(answ=='o')
		{
			cout << "Great! Please choose sort's type: ";
			cout << "1: Bubble sort" << endl << "2: Bubble sort with the 1st Iverson's condition" << endl << "3: Bubble sort with the 2nd Iverson's condition" << endl;
			cin >> type;
			copy = copyArr(array, size);
			switch(type)
			{
			case 1:
				bubble(copy, size);
				printArr(copy, size);
				break;
			case 2:
					bubbleI1(copy, size);
					printArr(copy, size);
					break;
			case 3:
					bubbleI2(copy, size);
					printArr(copy, size);
			}
			cout << "Again?(y/n) ";cin >> answ;
			if(answ=='y')
			{
				cout << "New or old array?(n/o) ";cin >> answ;
			}
			else
				return 0;
		}
	}
}

int *copyArr(int* arr, int size)
{
	int *res = new int[size];
	for(int i=0;i<size;i++)
	{
		res[i]=arr[i];
	}
	return res;
}

bool checkArr(int *arr, int size)
{
	for(int i=0;i<size-1;i++)
	{
		if(arr[i] > arr[i+1])
			return false;
	}
	return true;
}

int* generateArr(int size, int min, int max)
{
	int* array = new int[size];
	srand (time(NULL));
	for(int i=0;i<size;i++)
	{
		array[i]=rand() % max + min;
	}
	return array;
}

void bubble(int *a, int n)
{
	int count = 0;
	for (int i=n-1;i>0;i--)
	{
		for (int j=0;j<i;j++)
		{
			if(a[j]>a[j+1])
			{
				int tmp=a[j];
				a[j]=a[j+1];
				a[j+1]=tmp;
				
			}
			count++;
		}
	}
}

void bubbleI1(int *a, int n)
{
	bool T = false;
	int count = 0;
	for (int i=n-1;i>0 && !T;i--)
	{
		T = true;
		for (int j=0;j<i;j++)
		{
			if(a[j]>a[j+1])
			{
				int tmp=a[j];
				a[j]=a[j+1];
				a[j+1]=tmp;
				T = false;
			}
			count++;
		}
	}
}

void bubbleI2(int *a, int n)
{
	int t = 0, bound = --n;
	int count = 0;
	for (int i=0;i<n && t!=-1;i++)
	{
		t = -1;
		for (int j=0;j<bound;j++)
		{
			if(a[j]>a[j+1])
			{
				t = j;
				int tmp=a[j];
				a[j]=a[j+1];
				a[j+1]=tmp;
			}
			count++;
		}
		if (t!=-1)
			bound = t;
	}
}

void printArr(int *arr, int size)
{
	for(int i = 0;i < size;i++)
	{
		cout << arr[i] << " ";
	}
	cout << endl << "Size: " << size << endl;
}
